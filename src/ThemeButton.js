import React from "react";
import { ThemeContext } from "./ThemeContext";


class ThemeButton extends React.Component {
  render() {
    return (
     <div className="theme-toggler">
       <select className={['select-component', this.context,].join(" ")} onChange={this.props.onChange}>
         <option value={this.context === ""}>Light Mode</option>
         <option value={this.context === "dark"}>Dark Mode</option>
       </select>
       </div>
    )
  }
}

export default ThemeButton;
ThemeButton.contextType = ThemeContext;