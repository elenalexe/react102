import React from "react";
import { ColorContext } from "./ColorContext";


class ColorButton extends React.Component {
  render() {
    return (
     <div className="color-toggler">
       <select className={['select-component', this.context,].join(" ")} onChange={this.props.onChange}>
         <option value={this.context === ""}>Blue Mode</option>
         <option value={this.context === "yellow"}>Yellow Mode</option>
       </select>
       </div>
    )
  }
}

export default ColorButton;
ColorButton.contextType = ColorContext;