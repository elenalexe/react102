
import './App.css';
import React from "react";
import ContextLand from './ContextLand';
import ThemeButton from './ThemeButton';
import { ThemeContext } from './ThemeContext';
import { ColorContext } from './ColorContext';
import ColorButton from './ColorButton';

class App extends React.Component {
  constructor(props) {
    super(props);
   this.state = {
    theme: "",
    color: "",
   }

   this.changeMode = () => {
    this.setState((state) => ({
      theme: state.theme === "" ? "dark" : "",
    }))
  }
  
   this.changeColor = () => {
    this.setState((state) => ({
      color: state.color === "" ? "yellow" : "",
    }))
  }
  }


  render() {
    console.log(this.state.theme);
    return (
      <ThemeContext.Provider value={this.state.theme}>
        <ColorContext.Provider value={this.state.color}>
          <div className='App'>
            <ThemeButton onChange={this.changeMode}></ThemeButton>
            <ColorButton onChange={this.changeColor}></ColorButton>
            <ContextLand />
          </div>
        </ColorContext.Provider>
      </ThemeContext.Provider>
    );
  }
}

export default App;
