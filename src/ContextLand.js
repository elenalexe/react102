
import './App.css';
import { ThemeContext } from "./ThemeContext";
import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink } from '@fortawesome/free-solid-svg-icons';

class ContextLand extends React.Component {
  render() {
    return (
      <div className="context-land">
        <div className={['card-component', this.context,].join(" ")}>
          <h1 className='title'>This is a card</h1>
          <p className='card-paragraph'>Hello card!</p>
        </div>
        <div className='form-component'>
          <h2 className='title'>This should be a form</h2>
          <label className='input-label'>Username:</label>
          <input className={['input-component', this.context,].join(" ")} type="text" name="name-input"></input>
          <button className={['button-component', this.context, ].join(" ")}>OK</button>
        </div>
        <div className={['table-component', this.context,].join(" ")}>
        <h3 className='title'>This is a table</h3>
        <table className='table'>
          <tr>
            <th>Name</th>
            <th>Tiredness</th>
          </tr>
          <tr>
            <th>_Hello</th>
            <th>40%</th>
          </tr>
          <tr>
            <th>_PinkPuffy</th>
            <th>100%</th>
          </tr>
        </table>
        </div>
        <div className='link-component'>
        <FontAwesomeIcon className={['awesome-icon', this.context,].join(" ")}icon={faLink} />
          <a className={["link", this.context,].join(" ")} href='#'>This is a link</a>
        </div>
        </div>
       
    );
  }
}

export default ContextLand;
ContextLand.contextType = ThemeContext;
